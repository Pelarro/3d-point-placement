﻿using UnityEngine;
namespace Points
{
    public class Segment
    {
        private LineRenderer _lineRenderer = null;
        private Point _firstPoint = null;
        private Point _secondPoint = null;
        private PointText _pointText = null;

        public Vector3 DirectionOrdered(Point point)
        {
            if (_firstPoint == point)
                return Direction;

            return _secondPoint.Position - _firstPoint.Position;
        }
        public Vector3 Direction { get { return _firstPoint.Position - _secondPoint.Position; } }
        public Point SetSecondPoint
        {
            set
            {

                _secondPoint = value;
            }
        }
        public void RemovePoint(Point point)
        {
            if (_firstPoint == point)
                _firstPoint = null;
            else if (_secondPoint == point)
                _secondPoint = null;
        }
        public void FirstPointFinished(Point firstPoint)
        {
            _firstPoint = firstPoint;
            _firstPoint.Segments.Add(this);
            _lineRenderer.enabled = true;
            _pointText.SetEnabled(true);
            _lineRenderer.SetPositions(new Vector3[] { _firstPoint.Position, _firstPoint.Position });
        }
        public void SetMissingPoint(Point point)
        {
            if (_firstPoint == null)
            {
                _firstPoint = point;
                point.AddSegment(this);
            }
            else if (_secondPoint == null)
            {
                _secondPoint = point;
                point.AddSegment(this);
            }

            SetDistance();
        }
        public void SecondPointFinished(Point secondPoint)
        {
            _secondPoint = secondPoint;
            _secondPoint.Segments.Add(this);
            SetDistance();
            
        }
        public void SetDegree(bool second = false)
        {
            if (_firstPoint != null)
                _firstPoint.SetDegree();
            if (_secondPoint != null)
                _secondPoint.SetDegree();
        }
        public void SetDistance()
        {
            _lineRenderer.SetPosition(0, _firstPoint.Position);
            _lineRenderer.SetPosition(1, _secondPoint.Position);
            _pointText.transform.position = ((_secondPoint.Position - _firstPoint.Position) * .5f) + _firstPoint.Position;
            _pointText.transform.position += Vector3.up * .2f;
            _pointText.SetDistance = Vector3.Distance(_firstPoint.Position, _secondPoint.Position);
        }
        public Segment(Point firstPoint, LineRenderer lineRenderer, PointText pointText)
        {
            _firstPoint = firstPoint;
            _lineRenderer = lineRenderer;
            _lineRenderer.enabled = false;
            _pointText = pointText;
            _pointText.SetEnabled(false);
        }
        public void Cleanup()
        {
            MonoBehaviour.Destroy(_lineRenderer);
            MonoBehaviour.Destroy(_pointText.gameObject);
            if (_firstPoint != null)
                MonoBehaviour.Destroy(_firstPoint.gameObject);
            if (_secondPoint != null)
                MonoBehaviour.Destroy(_secondPoint.gameObject);
        }
    }
}
