﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Points
{
    public class PointManager : MonoBehaviour
    {
        [SerializeField]
        private PointPlacement _pointPlacement = null;
        [SerializeField]
        private GameObject _pointPrefab = null;
        [SerializeField]
        private GameObject _linePrefab = null;
        [SerializeField]
        private GameObject _pointTextPrefab = null;

        [SerializeField]
        private Display _display = null;
        
        private List<Segment> _segments = new List<Segment>();
        private Segment _newSegment;

        public enum State { none, firstPoint, secondPoint, editPoint }
        private State _state = State.none;

        private Point _editPoint = null;
        private List<Segment> Segments = new List<Segment>();

        private void Awake()
        {
            SetStateOpen();
        }
        private void SetStateOpen()
        {
            _state = State.none;
            _pointPlacement.PointHitEnterEvent += PointHitEnter;
            _pointPlacement.PointHitExitEvent += PointHitExit;
            _display.Create();
        }
        private void PointHitEnter(Point point)
        {
            _display.Edit();
            point.SetEdit();
            _editPoint = point;
        }
        private void PointHitExit(Point point)
        {
            _display.Create();
            point.SetStandard();
            if(_state != State.editPoint)
                _editPoint = null;
        }
        private void Update()
        {
            if (_state == State.none)
            {
                if (_editPoint != null)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        Segments = _editPoint.Segments;
                        _pointPlacement.PlacePoint(_editPoint);
                        _pointPlacement.PointPlacedEvent += PointSet;
                        _state = State.editPoint;
                        _editPoint.SetStandard();
                        _display.Place();
                        return;
                    }
                }
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    if (_editPoint != null)
                        PointHitExit(_editPoint);
                    
                    _pointPlacement.PointHitEnterEvent -= PointHitEnter;
                    _pointPlacement.PointHitExitEvent -= PointHitExit;
                    
                    _newSegment = new Segment(CreateNewPoint(), (Instantiate(_linePrefab) as GameObject).GetComponent<LineRenderer>(),
                        (Instantiate(_pointTextPrefab) as GameObject).GetComponent<PointText>());

                    _display.Place();
                    _state = State.firstPoint;
                }
                return;
            }
            
            _pointPlacement.SetPointUpdate();
        }
        private void LateUpdate()
        {
            if (_state == State.secondPoint)
            {
                _newSegment.SetDegree();
                _newSegment.SetDistance();
            }
            else if(_state == State.editPoint)
            {
                for(int i = 0; i<Segments.Count; i++)
                {
                    Segments[i].SetDistance();
                    Segments[i].SetDegree();
                }
            }
        }
        private void PointSet(Point point)
        {
            _pointPlacement.PointPlacedEvent -= PointSet;
            if (_state == State.firstPoint)
            {
                _state = State.secondPoint;
                _newSegment.FirstPointFinished(point);
                _newSegment.SetSecondPoint = CreateNewPoint();
                return;
            }
            else if (_state == State.secondPoint)
            {
                _newSegment.SecondPointFinished(point);
                _segments.Add(_newSegment);
                _newSegment.SetDegree(true);
                _newSegment = null;
            }
            else if(_state == State.editPoint)
            {
                for(int i = 0; i<Segments.Count; i++)
                {
                    Segments[i].SetMissingPoint(point);
                    Segments[i].SetDegree();
                }
            }

            SetStateOpen();
        }

        private Point CreateNewPoint()
        {
            Point point = (Instantiate(_pointPrefab) as GameObject).GetComponent<Point>();
            _pointPlacement.PlacePoint(point);
            _pointPlacement.PointPlacedEvent += PointSet;
            return point;
        }
    }
}