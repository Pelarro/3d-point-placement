﻿using UnityEngine;
using UnityEngine.Events;

namespace Points
{
    public class PointPlacement:MonoBehaviour
    {
        public UnityAction<Point> PointHitEnterEvent;
        public UnityAction<Point> PointHitExitEvent;
        public UnityAction<Point> PointPlacedEvent;
        [SerializeField]
        private Transform _pointLocation = null;

        public Vector3 PointPosition { get { return _pointToPlace.Position; } }

        private Point _pointToPlace = null;
        
        public void SetPointUpdate()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Point point = _pointToPlace;
                
                if (_pointToPlace.Connection != null)
                {
                    point = _pointToPlace.Connection;
                    point.SetStandard();
                    _pointToPlace.CleanFromSegments();
                    Destroy(_pointToPlace.gameObject);
                }
                _pointToPlace = null;
                point.transform.SetParent(null);
                if (PointPlacedEvent != null)
                    PointPlacedEvent(point);
            }
        }
        public void PlacePoint(Point point)
        {
            _pointToPlace = point;
            _pointToPlace.transform.SetParent(_pointLocation);
            _pointToPlace.transform.localPosition = Vector3.zero;
        }

        public void DestroyPoint()
        {
            if (_pointToPlace != null)
            {
                Destroy(_pointToPlace.gameObject);
                _pointToPlace = null;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("POINT HIT STUFF");
            if (_pointToPlace == null)
            {
                if (PointHitEnterEvent != null)
                    PointHitEnterEvent(other.GetComponent<Point>());
                return;
            }

            if (other.gameObject == _pointToPlace.gameObject)
                return;

            Point point = other.GetComponent<Point>();
            if (point == null || !point.CanConnect || !_pointToPlace.CanConnect)
                return;
            point.SetShared();
            _pointToPlace.Connection = point;
            _pointToPlace.transform.SetParent(point.transform);
            _pointToPlace.transform.localPosition = Vector3.zero;
            _pointToPlace.gameObject.SetActive(false);
        }
        private void OnTriggerExit(Collider other)
        {
            if (_pointToPlace == null)
            {
                if (PointHitExitEvent != null)
                    PointHitExitEvent(other.GetComponent<Point>());
                return;
            }
            if (other.gameObject == _pointToPlace.gameObject || _pointToPlace.Connection == null)
                return;
            
            _pointToPlace.Connection.SetStandard();
            _pointToPlace.Connection = null;
            _pointToPlace.transform.SetParent(_pointLocation);
            _pointToPlace.transform.localPosition = Vector3.zero;
            _pointToPlace.gameObject.SetActive(true);
        }
    }
}
