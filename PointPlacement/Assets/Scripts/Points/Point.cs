﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Points
{
    public class Point:MonoBehaviour
    {
        [SerializeField]
        private TextMeshPro _degreeText = null;
        [SerializeField]
        private Material _standard = null;
        [SerializeField]
        private Material _shared = null;
        [SerializeField]
        private Material _edit = null;

        private Renderer _renderer;
        public Vector3 Position { get { return transform.position; } }

        public bool CanConnect { get { return Segments.Count < 2; } }
        public List<Segment> Segments = new List<Segment>();
        public Point Connection { get; set; }

        private void Awake()
        {
            _renderer = GetComponent<Renderer>();
        }
        public void CleanFromSegments()
        {
            for(int i = 0; i<Segments.Count; i++)
            {
                Segments[i].RemovePoint(this);
            }
        }
        public void AddSegment(Segment segment)
        {
            if (!CanConnect || Segments.Contains(segment))
                return;

            Segments.Add(segment);
        }
        public void SetOnConnection()
        {
            if (Connection == null)
            {
                transform.localPosition = Vector3.zero;
                return;
            }

            transform.position = Connection.Position;
        }
        public void SetDegree()
        {
            if (Segments.Count > 1)
            {
                float angle = Vector3.Angle(Segments[0].DirectionOrdered(this), Segments[1].DirectionOrdered(this));

                _degreeText.text = string.Format("{0}°", angle.ToString("0.0"));
                _degreeText.transform.SetParent(null);

                Vector3 loc = Position + Segments[0].DirectionOrdered(this).normalized * .3f;
                Debug.DrawRay(Position, loc, Color.blue, 1);
                Vector3 other = Position + Segments[1].DirectionOrdered(this).normalized * .3f;
                Debug.DrawRay(Position, other, Color.red, 1);

                _degreeText.transform.position = loc - ((loc - other) * .5f);
                Debug.DrawRay(loc, loc - other, Color.black, 1);
            }
        }
        public void SetStandard()
        {
            _renderer.material = _standard;
        }
        public void SetShared()
        {
            _renderer.material = _shared;
        }
        public void SetEdit()
        {
            _renderer.material = _edit;
        }
    }
}
