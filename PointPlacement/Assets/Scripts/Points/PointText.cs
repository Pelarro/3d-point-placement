﻿using UnityEngine;
using TMPro;
using System;

namespace Points
{
    public class PointText:MonoBehaviour
    {
        [SerializeField]
        private TextMeshPro _text = null;

        public Transform LookAt { get; set; }

        private void Awake()
        {
            LookAt = Camera.main.transform;
        }

        public void SetEnabled(bool value)
        {
            _text.enabled = value;
        }
        public float SetDistance
        {
            set
            {
                _text.text = value.ToString("0.0");
            }
        }

        private void Update()
        {
            if (LookAt == null)
                return;

            transform.LookAt(LookAt);
        }

    }
}
