﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CameraMovement
{
    public class KeyboardMouse : MonoBehaviour
    {
        [SerializeField]
        private float _speed = 10;
        
        [SerializeField]
        private float _rotateionSpeed = 10;

        private void Update()
        {
            if (Input.GetKey(KeyCode.W))
            {
                transform.position += transform.forward * _speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.position -= transform.right * _speed * Time.deltaTime;
            }
            else if (Input.GetKey(KeyCode.D))
            {
                transform.position += transform.right * _speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.position -= transform.forward * _speed * Time.deltaTime;
            }

            if (Input.GetMouseButton(1))
            {
                Vector3 rot = new Vector3(-Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"), 0) * _rotateionSpeed;
                transform.Rotate(rot * Time.deltaTime);
                Vector3 temp = transform.eulerAngles;
                temp.z = 0;
                transform.eulerAngles = temp;
            }
        }
    }
}
