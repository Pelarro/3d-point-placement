﻿using UnityEngine;

[System.Serializable]
public class Display
{
    [SerializeField]
    private GameObject _create = null;
    [SerializeField]
    private GameObject _edit = null;
    [SerializeField]
    private GameObject _place = null;

    public void Create()
    {
        _create.SetActive(true);
        _edit.SetActive(false);
        _place.SetActive(false);
    }
    public void Edit()
    {
        _edit.SetActive(true);
        _create.SetActive(false);
        _place.SetActive(false);
    }
    public void Place()
    {
        _place.SetActive(true);
        _edit.SetActive(false);
        _create.SetActive(false);
    }
}

